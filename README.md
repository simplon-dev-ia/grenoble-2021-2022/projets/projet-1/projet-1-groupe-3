# Projet 1 - Groupe 3


# LE MODÈLE

Le code du modèle et du pipeline ce situe dans le fichier jupyter notebook intitulé "Modele_nutriscore.ipynb". 
Le modèle peut être utiliser lui grâce au fichier binaire jobelib intitulé "pipeline_model_nutriscore.joblib" en l'important au préalable.

## Prédiction
Afin d'effectuer une prédiction avec le modèle il faut faire appel à la méthode .predict. Retourne une liste d'un élément qui contient la classe prédite.

l'ordre des attributs pour la prédiction et le suivant : 

- Kcal (g/100g)
- Matière Grasse (g/100g)
- Gras saturé (g/100g)
- Protéine (g/100g)
- Sucre (g/100g)
- Sel (g/100g)
- Fibre (g/100g)
- Fruit à coque (%/100g)
- Glucide (g/100g)

exemple d'utilisation : 

```py
from joblib import load

model = load("pipeline_model_nutriscore.joblib")

exemple_prediction = [[397,10,4.2,11,9.1,0.08,9.5,0,61]]

model.predict(exemple_prediction)
# >>> ['b']
```