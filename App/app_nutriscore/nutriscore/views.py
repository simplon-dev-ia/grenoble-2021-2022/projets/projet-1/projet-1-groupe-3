from multiprocessing import context
from operator import mod
from unittest import mock
from django.shortcuts import render, redirect
from .forms import Nutriscore_1, Nutriscore_2
from joblib import load

# Create your views here.
nutriscore_model_v2 = load('model_nutriscore_v2.joblib')
nutriscore_model_v2_bis = load('model_nutriscore_v2_less_features.joblib')


def home_page(request):

    if request.method == 'POST':
        model_choice = request.POST.get('model_choice')

        if model_choice == 'model_1':
            form1 = Nutriscore_1(request.POST, prefix='form1')
            form2 = Nutriscore_2(prefix='form2')

            if form1.is_valid():
                data = form1.cleaned_data
                note_nutri_score = nutriscore_model_v2.predict([[
                    data['kcal'], data['matiere_grasse'], data['gras_sature'],
                    data['glucide'], data['sucre'], data['proteine'], data['sel'], data['fibre']
                ]])
                return redirect(f"resultat/{note_nutri_score[0]}")

        elif model_choice == 'model_2':
            form1 = Nutriscore_1(prefix='form1')
            form2 = Nutriscore_2(request.POST, prefix='form2')

            if form2.is_valid():
                data = form2.cleaned_data
                note_nutri_score = nutriscore_model_v2_bis.predict([[
                    data['kcal'], data['gras_sature'], data['sucre'],
                    data['sel'], data['fibre']
                ]])
                print(note_nutri_score)
                return redirect(f"resultat/{note_nutri_score[0]}")

    else:
        form1 = Nutriscore_1(prefix='form1')
        form2 = Nutriscore_2(prefix='form2')

    return render(request, 'nutriscore/home.html', {
        'form1': form1,
        'form2': form2,
    })

def resultat(request, note):
    note_nutri = note
    context = {
        "note" : note_nutri,
    }
    return render(request, 'nutriscore/resultat.html', context = context)

def a_propos(request):
   
    return render(request, 'nutriscore/a_propos.html')