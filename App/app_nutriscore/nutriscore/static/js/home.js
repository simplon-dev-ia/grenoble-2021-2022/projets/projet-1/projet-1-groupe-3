function updateFieldsVisibility() {
  const modelChoice = document.getElementById("model_choice");
  const model1Fields = document.getElementById("model_1_fields");
  const model2Fields = document.getElementById("model_2_fields");

  if (modelChoice.value === "model_1") {
    model1Fields.style.display = "block";
    model2Fields.style.display = "none";
  } else if (modelChoice.value === "model_2") {
    model1Fields.style.display = "none";
    model2Fields.style.display = "block";
  }
}

function validateForm() {
  const modelChoice = document.getElementById("model_choice");
  const model1Fields = document.getElementById("model_1_fields");
  const model2Fields = document.getElementById("model_2_fields");
  let isValid = true;

  if (modelChoice.value === "model_1") {
    model1Fields.querySelectorAll("input").forEach((input) => {
      if (!input.checkValidity()) {
        isValid = false;
      }
    });
  } else if (modelChoice.value === "model_2") {
    model2Fields.querySelectorAll("input").forEach((input) => {
      if (!input.checkValidity()) {
        isValid = false;
      }
    });
  }

  return isValid;
}

// Update fields visibility on page load
updateFieldsVisibility();

// Update fields visibility when the model choice changes
document
  .getElementById("model_choice")
  .addEventListener("change", updateFieldsVisibility);

// Add a submit event listener to validate the visible form fields
document.querySelector("form").addEventListener("submit", (e) => {
  if (!validateForm()) {
    e.preventDefault();
  }
});
