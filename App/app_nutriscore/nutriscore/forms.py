from django import forms

class Nutriscore_1(forms.Form):    
    kcal = forms.FloatField(label='Kcal (g/100g)')
    matiere_grasse = forms.FloatField(label='Matière grasse (g/100g)')
    gras_sature = forms.FloatField(label='Gras saturé (g/100g)')
    glucide = forms.FloatField(label='Glucide g/100g')
    sucre = forms.FloatField(label='Sucre (g/100g)')
    fibre = forms.FloatField(label='Fibre (g/100g)')
    proteine = forms.FloatField(label='Protéine (g/100g)')
    sel = forms.FloatField(label='Sel (g/100g)')

class Nutriscore_2(forms.Form):
    kcal = forms.FloatField(label='Kcal (g/100g)')
    gras_sature = forms.FloatField(label='Gras saturé (g/100g)')
    sucre = forms.FloatField(label='Sucre (g/100g)')
    sel = forms.FloatField(label='Sel (g/100g)')
    fibre = forms.FloatField(label='Fibre (g/100g)')