from django.urls import path
from django.conf.urls import include
from . import views


urlpatterns =[
    path('',views.home_page, name='home'),
    path('resultat/<str:note>/',views.resultat, name='resultat'),
    path('a_propos',views.a_propos, name='a_propos'),
    


]