from django.apps import AppConfig


class NutriscoreConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'nutriscore'
